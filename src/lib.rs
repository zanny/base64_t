//! A base64 type based on [base64](https://crates.io/crates/base64) that cannot error.
//!
//! ```
//! let encoded = base64_t::Base64::new("Test String");
//! assert_eq!(encoded.decode(), "Test String".to_string().into_bytes());
//! ```
extern crate base64;

/// Configuration types imported from ['base64'](https://docs.rs/base64).
pub use base64::{Config, MIME, STANDARD, STANDARD_NO_PAD, URL_SAFE, URL_SAFE_NO_PAD};

/// Typesafe base64 encoded bytes with an encoding configuration.
pub struct Base64 {
    data: Vec<u8>,
    config: Config,
}

impl Base64 {
    /// Takes the same types `base64::encode`.
    /// Default encoding is [STANDARD](constant.STANDARD.html)
    pub fn new<T: ?Sized + AsRef<[u8]>>(input: &T) -> Self {
        Base64::with_config(input, STANDARD)
    }

    /// Same as new, just specify a non-STANDARD `Config`.
    pub fn with_config<T: ?Sized + AsRef<[u8]>>(input: &T, config: Config) -> Self {
        let data = base64::encode_config(input, config.clone());
        Base64 {
            data: data.into_bytes(),
            config,
        }
    }

    /// Decodes the encoded Base64, and cannot fail - you can only make a Base64 by encoding data.
    pub fn decode(&self) -> Vec<u8> {
        base64::decode_config(&self.data, self.config.clone()).unwrap_or_else(|_| {
            unreachable!("Decoding CANNOT fail because we only store encoded base64")
        })
    }

    /// Reference to the `Config` used to initalize this Base64.
    pub fn config(&self) -> &Config {
        &self.config
    }

    /// Reference to the encoded bytes, useful when you cannot send the whole struct somewhere.
    pub fn encoded(&self) -> &Vec<u8> {
        &self.data
    }
}

#[cfg(test)]
mod tests {
    use Base64;

    #[test]
    fn strings() {
        let string = "test string";
        let b64 = Base64::new(string);
        let decoded = b64.decode();
        let result = String::from_utf8_lossy(&decoded[..]);
        assert_eq!(result, string);
    }

    #[test]
    fn bytes() {
        let bytes = b"test bytes";
        let b64 = Base64::new(bytes);
        let decoded = b64.decode();
        assert_eq!(&decoded[..], bytes);
    }

    #[test]
    fn nums() {
        let num = 3525623682623862362.252352242f64;
        let stringify = num.to_string();
        let b64 = Base64::new(&stringify);
        let decoded = b64.decode();
        let result = String::from_utf8_lossy(&decoded[..]);
        assert_eq!(num, result.parse().unwrap());
    }
}
