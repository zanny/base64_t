# base64_t

A Base64 type based on [base64](https://crates.io/crates/base64) that guarantees successful
encoding and decoding.

## [Documentation](https://docs.rs/base64_t)

Licensed under the Apache License 2.0 ([LICENSE](LICENSE) or  http://www.apache.org/licenses/LICENSE-2.0)
